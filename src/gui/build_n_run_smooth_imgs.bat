echo removing previous binrs, objs and imgs
del /f  anaelu_img_smooth.exe img_smooth.edf
del /f  anaelu_img_smooth img_smooth.edf
del /f *.o *.mod

echo compiling
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall tools.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall anaelu_img_smooth.f90

echo linking
gfortran -o anaelu_img_smooth.exe *.o -static

echo running binary exe
rem anaelu_img_smooth.exe cuted.edf img_smooth.edf 5

del /f  *.o *.mod
