import sys
import os
from PySide2 import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *
import subprocess
import fabio
import numpy as np
from img_rgb_gen import np2bmp_heat, np2bmp_diff
from grid_atoms_xyz import read_cfl_atoms, AtomData
from readcfl import read_cfl_file, CflData
from readdat import read_dat_file, InstrumentData
from exluding_marking import exlud_mark
from showing_cuts import show_cut
import time

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    m_moving = Signal(float, float)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)
        self.img_arr = None
        self.i_max = None
        self.i_min = None

    def open_img(self, img_path):
        self.img_arr = fabio.open(img_path).data.astype("float64")
        self.i_max = np.max(self.img_arr)
        self.i_min = np.min(self.img_arr)

    def get_min_max(self):
        return self.i_min, self.i_max

    def wheelEvent(self, event):
        int_delta = int(event.delta())
        self.m_scrolling.emit(int_delta)
        event.accept()

    def mouseMoveEvent(self, event):
        ev_pos = event.scenePos()
        x_pos, y_pos = float(ev_pos.x()), float(ev_pos.y())
        self.m_moving.emit(x_pos, y_pos)
        event.accept()


class Form(QObject):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)

        self.main_q_obj_path = os.path.dirname(os.path.abspath(__file__))

        ui_path = self.main_q_obj_path + os.sep + "Anaelu_gui.ui"
        print("ui_path = ", ui_path)
        self.window = QtUiTools.QUiLoader().load(ui_path)
        icon_path = self.main_q_obj_path + os.sep + "anaelu.png"
        self.window.setWindowTitle("ANAELU 2.0")
        self.window.setWindowIcon(QIcon(icon_path))

        #path
        bin_exe_path = os.path.abspath(__file__)[0:-23] + "tmp_exe"
        print("file path = ", bin_exe_path,"\n")
        #sys.path += [bin_exe_path]
        os.environ["PATH"] += os.pathsep + bin_exe_path
        #print("sys path = ", sys.path,"\n")
        print("environ path = ", os.environ["PATH"],"\n")

        self.run_tmp_path = os.path.abspath(__file__)[0:-27] + "tmp_files"
        print("exes path = ", self.run_tmp_path, "\n")
        os.chdir(self.run_tmp_path)
        print("changed directory =", self.run_tmp_path, "\n")

        self.examp_path = os.path.abspath(__file__)[0:-27] + "Example"
        print("examp_path =", self.examp_path)

        self.curent_zoom_scale = 1.0
        self.gscene_list = []

        for n in range(4):
            new_gsecene = MultipleImgSene()
            new_gsecene.m_scrolling.connect(self.scale_all_views)
            new_gsecene.m_moving.connect(self.moving_mouse)
            self.gscene_list.append(new_gsecene)

        self.gview_list = [
            self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
            self.window.graphicsView_Modl, self.window.graphicsView_Bakgr
        ]

        for n, gview in enumerate(self.gview_list):
            gview.setScene(self.gscene_list[n])

        self.set_drag_mode()

        self.tot_imin = 0
        self.tot_imax = 0

        #utilities for buttons bottom part
        self.window.run_model.clicked.connect(self.run_model_connect)
        self.window.actionRun_Model.triggered.connect(self.run_model_connect)

        self.window.pushButton_mask_model.clicked.connect(self.onmask_run)
        self.window.actionMask_Model.triggered.connect(self.onmask_run)

        self.window.cut_mask_peaks.clicked.connect(self.cut_mask_run)
        self.window.actionCut_Mask_Peaks.triggered.connect(self.cut_mask_run)

        self.window.background_smoothing.clicked.connect(self.smooth_run)
        self.window.actionSmooth_Background.triggered.connect(self.smooth_run)

        self.window.scale_sum_back_diff.clicked.connect(
            self.scale_sum_back_diff_connect
        )
        self.window.actionSum_Background.triggered.connect(
            self.scale_sum_back_diff_connect
        )
        self.window.remodel_comparison.clicked.connect(self.on_remodel)
        self.window.actionRe_model.triggered.connect(self.on_remodel)

        self.window.pop_mark_tool.clicked.connect(self.on_ex_mark_run)
        self.window.rotate_tool.clicked.connect(self.rot_img)

        #Add row delete row Table Widget
        self.window.del_row_button.clicked.connect(self.remove_r)
        self.window.add_row_button.clicked.connect(self.add_r)

        #open read cfl Tabs
        self.window.actionOpen_Cfl.triggered.connect(
            self.actionOpen_Cfl_connect
        )
        self.window.actionSave_Cfl.triggered.connect(self.savefile_cfl)

        #open read dat And write Dat
        self.window.actionOpen_Dat.triggered.connect(
            self.actionOpen_Dat_connect
        )
        self.window.actionSave_Dat.triggered.connect(self.savefile_dat)
        #Load image action
        self.window.actionOpen_Experimental_Image.triggered.connect(
            self.load_exp_img
        )
        #Update palettes for images
        self.window.sqrt_checkbox.stateChanged.connect(self.draw_3_imgs)
        self.window.invert_colours_checkbox.stateChanged.connect(
            self.draw_3_imgs
        )
        self.window.green_blue_palette_checkbox.stateChanged.connect(
            self.draw_left_down_img
        )
        self.window.dark_bright_palette_checkbox.stateChanged.connect(
            self.draw_left_down_img
        )

        #Choose what ro show on bottom left image
        self.window.MaskOrDiffRbut.toggled.connect(self.left_down_img_change)
        self.window.RadCutRbut.toggled.connect(self.left_down_img_change)
        self.window.AzmCutRbut.toggled.connect(self.left_down_img_change)
        self.l_down = "msk_dif"

        #plain text to modify for the user
        self.window.mask_treshold_edit.setText("0.5")
        self.window.times_smooth_edit.setText("15")
        self.window.scale_factor_edit.setText("1.0")
        self.window.residual_edit.setText("None")

        #colums table widget
        columns = ['z','Label','Xpos','Ypos','Zpos','DWF']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)

        #Tooltip add
        self.window.run_model.setToolTip("Calculate theoretical 2D-XRD")
        self.window.pushButton_mask_model.setToolTip("Calculate mask")
        self.window.cut_mask_peaks.setToolTip(
            "Obtain background by cutting peaks"
        )
        self.window.background_smoothing.setToolTip(
            "Indicate number of iterations for smoothing background"
        )
        self.window.scale_sum_back_diff.setToolTip(
            "Calculate difference between experimental and calculated 2D-XRD"
        )
        self.window.remodel_comparison.setToolTip(
            "Calculate new 2D-XRD and compare"
        )
        self.window.pop_mark_tool.setToolTip("Blackout area of detectors")
        #self.window.load_exp.setToolTip("Load experimental image")

        for gview in self.gview_list:
            gview.verticalScrollBar().valueChanged.connect(
                self.scroll_all_v_bars)
            gview.horizontalScrollBar().valueChanged.connect(
                self.scroll_all_h_bars)

        self.real_time_zoom = False
        self.value_backup_h = 0
        self.value_backup_v = 0

        self.bmp_heat = np2bmp_heat()
        self.bmp_diff = np2bmp_diff()
        self.add = 1

        self.window.show()

    def left_down_img_change(self):
        print("left_down_img_change(?)")
        # deliberately redundant if's for clarification
        if bool(self.window.MaskOrDiffRbut.isChecked()):
            self.l_down = "msk_dif"
            self.set_drag_mode()

        elif bool(self.window.RadCutRbut.isChecked()):
            self.l_down = "rad_cut"
            self.set_NO_drag_mode()
            print("left_down_img_change here 1")
            self.cut_diag = show_cut()
            self.cut_diag.leaving.connect(self.diff_uncheck)
            print("left_down_img_change here 2")


        elif bool(self.window.AzmCutRbut.isChecked()):
            self.l_down = "azm_cut"
            self.set_NO_drag_mode()

        print("self.l_down =", self.l_down)#
        #
    def diff_uncheck(self):
        print("diff_uncheck")
        self.window.MaskOrDiffRbut.setChecked(True)
        self.cut_diag.dialog.close()

    def set_drag_mode(self):
        for gview in self.gview_list:
            gview.setDragMode(QGraphicsView.ScrollHandDrag)

    def set_NO_drag_mode(self):
        for gview in self.gview_list:
            gview.setDragMode(QGraphicsView.NoDrag)

    def moving_mouse(self, x, y):
        if self.l_down == "rad_cut" or self.l_down == "azm_cut":
            #print("moving_mouse:", x, y)
            x_bm = float(self.window.x_beam_edit.text())
            y_bm = float(self.window.y_beam_edit.text())
            self.draw_3_pix_maps()
            self.draw_left_down_cut(x_bm, y_bm, x, y)

    def draw_left_down_cut(self, x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        x_to, y_to = x1, y1
        x_res = int(self.window.x_res_edit.text())
        y_res = int(self.window.y_res_edit.text())
        extra_long = x_res + y_res
        exp_arr_cut = np.zeros((extra_long,), dtype = float)
        cal_arr_cut = np.zeros((extra_long,), dtype = float)
        bak_arr_cut = np.zeros((extra_long,), dtype = float)
        dif_arr_cut = np.zeros((extra_long,), dtype = float)
        arr_x = np.zeros((extra_long,), dtype = int)
        arr_y = np.zeros((extra_long,), dtype = int)
        if abs(dx) > abs(dy):
            dy_uni = dy / dx
            l_max = int(abs(dx))
            for x_posit in range(l_max):
                if x2 > x1:
                    x_to = x1 + x_posit
                    y_to += dy_uni

                else:
                    x_to = x1 - x_posit
                    y_to -= dy_uni

                if(
                    x_to > x_res - 2 or y_to > y_res - 2
                    or x_to < 1 or y_to < 1
                ):
                    l_max = old_x_pos
                    break

                arr_x[x_posit] = int(x_to)
                arr_y[x_posit] = int(y_to)

                old_x_pos = x_posit

        else:
            dx_uni = dx / dy
            l_max = int(abs(dy))
            for y_posit in range(l_max):
                if y2 > y1:
                    y_to = y1 + y_posit
                    x_to += dx_uni

                else:
                    y_to = y1 - y_posit
                    x_to -= dx_uni

                if(
                    x_to > x_res - 2 or y_to > y_res - 2
                    or x_to < 1 or y_to < 1
                ):
                    l_max = old_y_pos
                    break

                arr_x[y_posit] = int(x_to)
                arr_y[y_posit] = int(y_to)

                old_y_pos = y_posit
        try:
            for i in range(l_max):
                cal_arr_cut[i] = self.gscene_list[2].img_arr[arr_y[i], arr_x[i]]

        except TypeError:
            cal_arr_cut[:] = 0.0

        try:
            for i in range(l_max):
                exp_arr_cut[i] = self.gscene_list[0].img_arr[arr_y[i], arr_x[i]]

        except TypeError:
            exp_arr_cut[:] = 0.0

        try:
            for i in range(l_max):
                bak_arr_cut[i] = self.gscene_list[3].img_arr[arr_y[i], arr_x[i]]

        except TypeError:
            bak_arr_cut[:] = 0.0

        try:
            for i in range(l_max):
                dif_arr_cut[i] = self.gscene_list[1].img_arr[arr_y[i], arr_x[i]]

        except TypeError:
            dif_arr_cut[:] = 0.0


        trim_cut_exp = exp_arr_cut[0:l_max]
        trim_cut_cal = cal_arr_cut[0:l_max]
        trim_cut_bak = bak_arr_cut[0:l_max]
        trim_cut_dif = dif_arr_cut[0:l_max]

        self.cut_diag.repaint(
            trim_cut_cal, trim_cut_exp, trim_cut_bak, trim_cut_dif
        )

        lin_pen = QPen(
            Qt.cyan, 3.8, Qt.SolidLine,
            Qt.RoundCap, Qt.RoundJoin
        )
        for num, tmp_gscene in enumerate(self.gscene_list):
            if num != 1:
                tmp_gscene.addLine(x1, y1, x_to, y_to, lin_pen)

    def run_model_connect(self):
        self.on_run()

    def actionOpen_Dat_connect(self):
        self.read_dat()

    def scale_sum_back_diff_connect(self):
        self.scale_run()
        self.imgadd_run()
        self.R_run()

    def on_remodel(self):
        self.run_model_connect()
        self.scale_sum_back_diff_connect()

    def actionOpen_Cfl_connect(self):
        self.read_cfl()

    def scroll_all_v_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.verticalScrollBar().setValue(value)

            self.value_backup_v = value

    def scroll_all_h_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.horizontalScrollBar().setValue(value)

            self.value_backup_h = value

    def scale_all_views(self, scale_fact):
        self.real_time_zoom = True
        for gview in self.gview_list:
            gview.scale(1.0 + float(scale_fact) / 2500.0,
                        1.0 + float(scale_fact) / 2500.0)

        self.real_time_zoom = False

        self.curent_zoom_scale = float(
            self.gview_list[0].transform().m11() +
            self.gview_list[0].transform().m22()
        ) / 2.0

    def emit_move(self):
        self.pos_img_n(self.num_to_move)

    def pos_img_n(self, gview_num):
        self.gview_list[gview_num].horizontalScrollBar().setValue(
            self.value_backup_h)
        self.gview_list[gview_num].verticalScrollBar().setValue(
            self.value_backup_v)

    def draw_3_pix_maps(self):
        for num, tmp_gscene in enumerate(self.gscene_list):
            if num != self.num_to_ignore and tmp_gscene.img_arr is not None:
                self.gscene_list[num].clear()
                self.gscene_list[num].addPixmap(self.lst_pixmap[num])

    def draw_3_imgs(self):
        self.tot_imin = 0
        self.tot_imax = 0
        self.num_to_ignore = 1
        for num, tmp_gscene in enumerate(self.gscene_list):
            imin, imax = tmp_gscene.get_min_max()
            if num != self.num_to_ignore and imin != None and imax != None:
                if imin < self.tot_imin:
                    self.tot_imin = imin

                if imax > self.tot_imax:
                    self.tot_imax = imax

        self.lst_pixmap = []
        for num, tmp_gscene in enumerate(self.gscene_list):
            if num != self.num_to_ignore and tmp_gscene.img_arr is not None:
                sqrt_stat = bool(self.window.sqrt_checkbox.isChecked())
                inve_stat = bool(
                    self.window.invert_colours_checkbox.isChecked()
                )

                rgb_np = self.bmp_heat.img_2d_rgb(
                    data2d = self.gscene_list[num].img_arr,
                    invert = inve_stat, sqrt_scale = sqrt_stat,
                    i_min_max = [self.tot_imin, self.tot_imax]
                )
                q_img = QImage(
                    rgb_np.data,
                    np.size(rgb_np[0:1, :, 0:1]),
                    np.size(rgb_np[:, 0:1, 0:1]),
                    QImage.Format_ARGB32
                )
                self.lst_pixmap.append(QPixmap.fromImage(q_img))
            else:
                self.lst_pixmap.append(None)

        self.draw_3_pix_maps()

    def draw_left_down_img(self):
        #if self.l_down == "msk_dif":

        inve_stat = bool(
            self.window.green_blue_palette_checkbox.isChecked()
        )
        sqrt_stat = bool(
            self.window.dark_bright_palette_checkbox.isChecked()
        )

        rgb_np = self.bmp_diff.img_2d_rgb(
            data2d = self.gscene_list[1].img_arr,
            invert = inve_stat, sqrt_scale = sqrt_stat,
        )
        self.gscene_list[1].clear()
        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )
        tmp_pixmap = QPixmap.fromImage(q_img)
        self.gscene_list[1].addPixmap(tmp_pixmap)

        #else:
        #    self.draw_cut()

    def draw_cut(self):
        self.gscene_list[1].clear()
        lin_pen = QPen(
            Qt.blue, 0.8, Qt.SolidLine,
            Qt.RoundCap, Qt.RoundJoin
        )
        self.gscene_list[1].addLine(0,0,500,1000, lin_pen)

    def set_q_img(self, img_path, gscene_num):
        self.gscene_list[gscene_num].open_img(img_path)
        if gscene_num == 1:
            self.draw_left_down_img()

        else:
            self.gscene_list[gscene_num].get_min_max()
            self.draw_3_imgs()

        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)

    def updare_res_intro(self):
        width = np.size( self.gscene_list[0].img_arr[0:1, :] )
        height = np.size( self.gscene_list[0].img_arr[:, 0:1] )
        self.window.x_res_edit.setText(str(width))
        self.window.y_res_edit.setText(str(height))
        if self.window.x_beam_edit.text() == "":
            self.window.x_beam_edit.setText(str(width / 2))
            self.window.y_beam_edit.setText(str(height / 2))

    def load_exp_img(self):
        print("self.btn_clk")
        try:
            file_in_path = QFileDialog.getOpenFileName(
                parent = self.window,
                caption = "Open Image File",
                dir = self.examp_path,
                filter = "Files (*.*)"
            )[0]
            img_in = fabio.open(file_in_path).data.astype(np.float64)

            res_img = fabio.edfimage.edfimage()
            res_img.data = img_in
            res_img.write(str("img_64bit_xrd.edf"))
            self.set_q_img("img_64bit_xrd.edf", 0)
            self.updare_res_intro()
            self.window.setWindowTitle(str(file_in_path))

        except FileNotFoundError:
            print("No file selected")

    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(
            self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(
            self.window.tableWidget_ini.model().rowCount())

        print(self.window.tableWidget_ini.model().rowCount())

    def read_cfl(self):
        try:
            file_in_path = QFileDialog.getOpenFileName(
                parent = self.window,
                caption = "Open CFL/CIF File",
                dir = self.examp_path,
                filter = "Files (*.cfl *.cif)"
            )[0]

            print("info from clf", file_in_path)

            if file_in_path[-3:] == "cif":
                print("Time to convert CIF file to CFL")
                in_cif_str = "cif_in=" + file_in_path
                out_cfl_str = "cfl_out=" + file_in_path[0:-3] + "cfl"

                print("in_cif_str = ",in_cif_str)
                print("out_cfl_str = ",out_cfl_str)

                subprocess.call(
                    ["anaelu_cif2cfl.exe", in_cif_str, out_cfl_str],
                    cwd = self.run_tmp_path
                )
                file_in_path = out_cfl_str[8:]

            self.data = read_cfl_file(my_file_path = file_in_path)

            self.window.title_edit.setText(str(self.data.title))
            self.window.spgr_edit.setText(str(self.data.spgr))
            self.window.a_edit.setText(str(self.data.a))
            self.window.b_edit.setText(str(self.data.b))
            self.window.c_edit.setText(str(self.data.c))
            self.window.a2_edit.setText(str(self.data.alpha))
            self.window.b2_edit.setText(str(self.data.beta))
            self.window.c2_edit.setText(str(self.data.gamma))
            self.window.sizelg_edit.setText(str(self.data.size_lg))
            self.window.ipfres_edit.setText(str(self.data.ipf_res))
            self.window.h_edit.setText(str(self.data.h))
            self.window.k_edit.setText(str(self.data.k))
            self.window.l_edit.setText(str(self.data.l))
            self.window.hklwh_edit.setText(str(self.data.hklwh))
            self.window.azmipf_edit.setText(str(self.data.azm_ipf))
            self.window.mask_treshold_edit.setText(str(self.data.mask_min))

            self.info2 = read_cfl_atoms(my_file_path = file_in_path)
            self.window.tableWidget_ini.setRowCount(len(self.info2))
            for n,atm in enumerate(self.info2):
                self.window.tableWidget_ini.setItem(
                    n,0,QTableWidgetItem(atm.Zn)
                )
                self.window.tableWidget_ini.setItem(
                    n,1,QTableWidgetItem(atm.Lbl)
                )
                self.window.tableWidget_ini.setItem(
                    n,2,QTableWidgetItem(atm.x)
                )
                self.window.tableWidget_ini.setItem(
                    n,3,QTableWidgetItem(atm.y)
                )
                self.window.tableWidget_ini.setItem(
                    n,4,QTableWidgetItem(atm.z)
                )
                self.window.tableWidget_ini.setItem(
                    n,5,QTableWidgetItem(atm.b)
                )

        except FileNotFoundError:
            print("No file selected")

    def savefile_cfl(self):
        self.cfl_file_name = QFileDialog.getSaveFileName(
            parent = self.window,
            caption = "Save CFL File",
            dir = self.examp_path,
            filter='*.cfl'
        )[0]

        print("save from cfl1=", self.cfl_file_name,"\n")
        self.write_out_cfl()

    def write_out_cfl(self):
        save_atm = []
        print(
            "range of table=",
            range(self.window.tableWidget_ini.rowCount()),"\n")

        for d in range(self.window.tableWidget_ini.rowCount()):
            print("print d= ", d)
            atm = AtomData()
            atm.Zn = str(self.window.tableWidget_ini.item(d, 0).text())
            atm.Lbl = str(self.window.tableWidget_ini.item(d, 1).text())
            atm.x = float(self.window.tableWidget_ini.item(d,2).text())
            atm.y = float(self.window.tableWidget_ini.item(d,3).text())
            atm.z = float(self.window.tableWidget_ini.item(d,4).text())
            atm.b = float(self.window.tableWidget_ini.item(d,5).text())

            save_atm.append(atm)
        print("data save_atom",save_atm)

        data = CflData()
        data.title = self.window.title_edit.text()
        data.spgr = self.window.spgr_edit.text()
        data.a = self.window.a_edit.text()
        data.b = self.window.b_edit.text()
        data.c = self.window.c_edit.text()
        data.alpha = self.window.a2_edit.text()
        data.beta = self.window.b2_edit.text()
        data.gamma = self.window.c2_edit.text()
        data.size_lg = self.window.sizelg_edit.text()
        data.ipf_res = self.window.ipfres_edit.text()
        data.h = self.window.h_edit.text()
        data.k = self.window.k_edit.text()
        data.l = self.window.l_edit.text()
        data.hklwh = self.window.hklwh_edit.text()
        data.azm_ipf = self.window.azmipf_edit.text()
        data.mask_min = self.window.mask_treshold_edit.text()

        crystal = "Title  "
        crystal += str(data.title) + " \n"
        crystal += "!" + "        a" + "        b" + "        c" + \
                "     alpha" + "    beta" + "   gamma" + "\n"
        crystal += "Cell    " + str(data.a) + "     " + str(data.b) +\
            "     " + str(data.c) + "  " + str(data.alpha) + "  " +\
            str(data.beta) + "  " + str(data.gamma) + "\n"
        crystal += "!     " + "Space Group" + "\n"
        crystal += "Spgr  " + str(data.spgr) + "\n"
        crystal += "SIZE_LG " + str(data.size_lg) + "\n"
        crystal += "IPF_RES " + str(data.ipf_res) + "\n"
        crystal += "HKL_PREF " + str(data.h) + " " + str(data.k) +\
            " " + str(data.l) + "\n"
        crystal += "HKL_PREF_WH " + str(data.hklwh) + "\n"
        crystal += "AZM_IPF " + str(data.azm_ipf) + "\n"

        if str(data.mask_min) != "None":
            crystal += "MASK_MIN " + str(data.mask_min) + "\n"

        crystal += "!" + "\n"

        cfl_data = str(crystal)
        try:
            self.info2 = open(self.cfl_file_name, 'w')
            for atom in save_atm:
                print("data of table=",atom.Zn,atom.Lbl,atom.x,atom.y,atom.z,atom.b,"\n")
                var = "Atom "
                var += str(atom.Zn) + "  "
                var += str(atom.Lbl) + "  "
                var += str(atom.x) + "  "
                var += str(atom.y) + "  "
                var += str(atom.z) + "  "
                var += str(atom.b) + " \n"
                cfl_data += str(var)

            self.info2.write(cfl_data)
            self.info2.close()

        except FileNotFoundError:
            print("No file selected")

    def read_dat(self):
        try:
            file_in_path = QFileDialog.getOpenFileName(
                parent = self.window,
                caption = "Open DAT File",
                dir = self.examp_path,
                filter = "Files (*.dat)"
            )[0]

            print("info from dat", file_in_path)
            dt = read_dat_file(my_file_path = file_in_path)
            self.window.x_res_edit.setText(str(dt.x_res))
            self.window.y_res_edit.setText(str(dt.y_res))
            self.window.x_beam_edit.setText(str(dt.x_beam))
            self.window.y_beam_edit.setText(str(dt.y_beam))
            self.window.wavelength_edit.setText(str(dt.lambd))
            self.window.sample_detector_distance_edit.setText(
                str(dt.dst_det)
            )
            self.window.detector_diameter_edit.setText(str(dt.diam_det))
            print("dt.up_hfl = ", dt.up_hfl)
            self.window.upper_half_only_check_box.setChecked(dt.up_hfl)

        except FileNotFoundError:
            print("No file selected")

    def savefile_dat(self):
        self.info_dat = QFileDialog.getSaveFileName(

            parent = self.window,
            caption = "Save DAT File",
            dir = self.examp_path,
            filter = '*.dat'
        )[0]

        print("save from dat=", self.info_dat,"\n")
        self.write_new_params()

    def write_new_params(self):
        dt = InstrumentData()
        dt.x_res = self.window.x_res_edit.text()
        dt.y_res = self.window.y_res_edit.text()
        dt.x_beam = self.window.x_beam_edit.text()
        dt.y_beam = self.window.y_beam_edit.text()
        dt.lambd = self.window.wavelength_edit.text()
        dt.dst_det = self.window.sample_detector_distance_edit.text()
        dt.diam_det = self.window.detector_diameter_edit.text()
        dt.up_hfl = bool(
            self.window.upper_half_only_check_box.checkState()
        )

        crystal = "xres" + "\n" + str(dt.x_res) + " \n"
        crystal += "yres"  + "\n" + str(dt.y_res) + "\n"
        crystal += "xbeam" + "\n" + str(dt.x_beam) + "\n"
        crystal += "ybeam" + "\n" + str(dt.y_beam) + "\n"
        crystal += "lambda" + "\n" + str(dt.lambd) + "\n"
        crystal += "dst_det" + "\n" + str(dt.dst_det) + "\n"
        ##### Temporal
        if dt.up_hfl == True:
            crystal += "UPPER_HALF" + "\n"
        ##### Temporal
        crystal += "diam_det" + "\n" +str(dt.diam_det) + "\n"
        ##### Temporal
        crystal += "thet_min" + "\n"+ "0" + "\n"
        ##### Temporal
        crystal += "end"

        dat_data = str(crystal)
        try:
            self.info2 = open(self.info_dat, 'w')
            self.info2.write(dat_data)
            self.info2.close()

        except FileNotFoundError:
            print("No file selected")

    def on_run(self):
        self.cfl_file_name = "out_Cfl.cfl"
        self.write_out_cfl()

        self.info_dat = "new_params.dat"
        self.write_new_params()

        subprocess.call(
            ["anaelu_calc_xrd.exe", "new_params.dat",
             "out_Cfl.cfl","img_xrd.edf", "raw_img_file.raw"],
            cwd = self.run_tmp_path
        )
        self.scale_run()
        self.set_q_img("scaled_img.edf", 2)

    def onmask_run(self):
        self.cfl_file_name = "out_Cfl.cfl"
        self.write_out_cfl()

        self.info_dat = "new_params.dat"
        self.write_new_params()

        subprocess.call(
            ["anaelu_calc_mask.exe", "new_params.dat",
             "out_Cfl.cfl", "img_mask.edf", "raw_img_file.raw"],
            cwd = self.run_tmp_path
        )
        self.set_q_img("img_mask.edf", 1)

    def cut_mask_run(self):

        subprocess.call(
            ["anaelu_img_cut.exe", "img_64bit_xrd.edf",
             "img_mask.edf", "cuted.edf"],
            cwd = self.run_tmp_path

        )
        self.set_q_img("cuted.edf", 3)

    def smooth_run(self):
        smooth_times = self.window.times_smooth_edit.text()
        smooth_func = ["anaelu_img_smooth.exe", "cuted.edf", "img_smooth.edf"]
        smooth_func.append(str(smooth_times))
        subprocess.call(smooth_func, cwd = self.run_tmp_path)

        self.set_q_img("img_smooth.edf", 3)

    def scale_run(self):
        scale_times = self.window.scale_factor_edit.text()
        scale_func = ["anaelu_img_scale.exe", "img_xrd.edf", "scaled_img.edf"]
        scale_func.append(str(scale_times))
        subprocess.call(scale_func, cwd = self.run_tmp_path)

    def imgadd_run(self):
        subprocess.call(
            ["anaelu_img_add.exe", "scaled_img.edf",
             "img_smooth.edf", "img_sum.edf"],
            cwd = self.run_tmp_path
        )
        self.set_q_img("img_sum.edf", 2)

    def R_run(self):
        subprocess.call(
            ["anaelu_dif_res.exe", "img_sum.edf",
             "img_64bit_xrd.edf", "img_diff.edf", "info_dif.dat"],
            cwd = self.run_tmp_path
        )

        my_file_path = "info_dif.dat"
        print ("my_file_path =", my_file_path)

        myfile = open(my_file_path, "r")

        all_lines = myfile.readlines()
        myfile.close()

        r_found = None
        r_2_found = None

        for lin_char in all_lines:
            commented = False
            for delim in ',;=':
                lin_char = lin_char.replace(delim, ' ')

            lst_litle_blocks = lin_char.split()
            for pos, litle_block in enumerate(lst_litle_blocks):

                if( litle_block == "!" or litle_block == "#" ):
                    commented = True

                elif( litle_block.upper() == "R" ):
                    print ("found r")
                    r_found = float(lst_litle_blocks[pos+1])

                elif( litle_block.upper() == "R_2" ):
                    print ("found r_2")
                    r_2_found = float(lst_litle_blocks[pos+1])

        print ("r_found =", r_found)
        print ("r_2_found =", r_2_found)
        self.window.residual_edit.setText(str(r_found))
        self.set_q_img("img_diff.edf", 1)

    def apply_ex_mark(self, excl_lst):
        print("excluding list =", excl_lst)

        for fig in excl_lst:
            print("\n", fig)
            if fig["figure"] == "rectangle":
                run_cmd_lst = [
                    "anaelu_img_ex_mark.exe", "img_64bit_xrd.edf",
                    "img_64bit_xrd.edf",
                    str(int(fig["x1"])) + "," + str(int(fig["x2"])) + "," +
                    str(int(fig["y1"])) + "," + str(int(fig["y2"])) ,
                    "rectangle"
                ]

            elif fig["figure"] == "circumference":
                run_cmd_lst = [
                    "anaelu_img_ex_mark.exe", "img_64bit_xrd.edf",
                    "img_64bit_xrd.edf",
                    str(int(fig["x1"])) + "," + str(int(fig["x2"])) + "," +
                    str(int(fig["y1"])) + "," + str(int(fig["y2"])) ,
                    "circumference"
                ]

            elif fig["figure"] == "ellipse":
                run_cmd_lst = [
                    "anaelu_img_ex_mark.exe", "img_64bit_xrd.edf",
                    "img_64bit_xrd.edf",
                    str(int(fig["x1"])) + "," + str(int(fig["x2"])) + "," +
                    str(int(fig["y1"])) + "," + str(int(fig["y2"])) ,
                    "ellipse"
                ]

            print("\n Running:", run_cmd_lst)
            subprocess.call(run_cmd_lst, cwd = self.run_tmp_path)
            self.set_q_img("img_64bit_xrd.edf", 0)

    def on_ex_mark_run(self):
        try:
            print("\nTime to pop exluding mask run")
            graphicsView = self.gview_list[0]
            b_rect = graphicsView.mapToScene(
                graphicsView.viewport().geometry()
            ).boundingRect()
            b_rect = b_rect.getRect()
            print("boundingRect = ", b_rect)

            x1, y1 = b_rect[0], b_rect[1]
            x2, y2 = x1 + b_rect[2], y1 + b_rect[3]
            col1, row1, col2, row2 = int(x1), int(y1), int(x2), int(y2)

            print("col1, row1, col2, row2 =", col1, row1, col2, row2)

            if col1 < 0:
                col1 = 0

            if row1 < 0:
                row1 = 0

            width = np.size( self.gscene_list[0].img_arr[0:1, :] )
            height = np.size( self.gscene_list[0].img_arr[:, 0:1] )

            if col2 > width:
                col2 = width

            if row2 > height:
                row2 = height

            print("col1, row1, col2, row2 =", col1, row1, col2, row2)

            sqrt_stat = bool(self.window.sqrt_checkbox.isChecked())
            inve_stat = bool(self.window.invert_colours_checkbox.isChecked())

            form = exlud_mark(
                self, self.gscene_list[0].img_arr[row1:row2,col1:col2],
                col1, row1, palette_stat = (sqrt_stat, inve_stat)
            )
            form.ended.connect(self.apply_ex_mark)

        except TypeError:
            print("no image there yet")

    def rot_img(self):
        subprocess.call(
            ["anaelu_rot_tool.exe", "img_64bit_xrd.edf", "img_64bit_xrd.edf"],
            cwd = self.run_tmp_path
        )
        self.set_q_img("img_64bit_xrd.edf", 0)
        self.updare_res_intro()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
