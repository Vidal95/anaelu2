rm anaelu_dif_res.exe info_dif.dat img_diff.edf
rm *.o *.mod

echo "compiling"
gfortran -c -O2 -Wall input_args.f90
gfortran -c -O2 -Wall rw_n_use_edf.f90
gfortran -c -O2 -Wall tools.f90

gfortran -c -O2 -Wall anaelu_dif_res.f90

echo "linking"
gfortran -o anaelu_dif_res.exe *.o -static

echo "running binary .exe"
./anaelu_dif_res.exe img_xrd_tot.edf img_smooth.edf img_diff.edf info_dif.dat

rm *.o *.mod


