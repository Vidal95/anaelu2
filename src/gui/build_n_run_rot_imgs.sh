echo "removing previous binrs, objs and imgs"
rm anaelu_rot_tool.exe img_rot.edf
rm *.o *.mod

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall rot_img.f90

echo "linking"
gfortran -o anaelu_rot_tool.exe *.o -static

echo "running binary exe"
./anaelu_rot_tool.exe img_64bit_xrd.edf img_rot.edf

rm *.o *.mod
