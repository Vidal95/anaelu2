import sys
import os
import numpy as np
import time

from PySide2 import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from img_rgb_gen import np2bmp_heat

class ExludImgagSene(QGraphicsScene):
    new_list = Signal(list)
    def __init__(self, parent = None):
        super(ExludImgagSene, self).__init__(parent)
        self.parent = parent
        print("__init__(ExludImgagSene)")
        self.x_ini = None
        self.y_ini = None
        self.pressed = False
        self.fig_lst = []

    def set_pix_map(self, pix_map, in_x, in_y):
        self.pix_map = pix_map
        self.corn_x, self.corn_y = in_x, in_y
        print("self.corn_x, self.corn_y =", self.corn_x, self.corn_y)

    def draw_pix_map(self):
        self.addPixmap(self.pix_map)

    def sheck_x1x2y1y2(self, x1, y1, x2, y2):
        if x1 > x2:
            x1, x2 = x2, x1

        if y1 > y2:
            y1, y2 = y2, y1

        return x1, y1, x2, y2

    def draw_single_rectangle(self, x1, y1, x2, y2):
        x1, y1, x2, y2 = self.sheck_x1x2y1y2(x1, y1, x2, y2)

        dx = x2 - x1
        dy = y2 - y1
        rectangle = QRectF(x1, y1, dx, dy)
        self.addRect(rectangle, self.green_pen)

    def draw_single_circumference(self, x1, y1, x2, y2):
        self.addLine(x1, y1, x2, y2, self.green_pen)
        dx = (x2 - x1) * 2.0
        dy = (y2 - y1) * 2.0
        r = np.sqrt(dx * dx + dy * dy)
        dx = r
        dy = r

        x1 = x1 - r / 2.0
        y1 = y1 - r / 2.0

        rectangle = QRectF(x1, y1, dx, dy)
        self.addEllipse(rectangle, self.green_pen)


    def draw_single_ellipse(self, x1, y1, x2, y2):
        x1, y1, x2, y2 = self.sheck_x1x2y1y2(x1, y1, x2, y2)
        dx = (x2 - x1)
        dy = (y2 - y1)

        rectangle = QRectF(x1, y1, dx, dy)
        self.addEllipse(rectangle, self.green_pen)

    def re_draw_me(self):
        self.clear()
        self.draw_pix_map()
        self.green_pen = QPen(
            Qt.green, 0.8, Qt.SolidLine,
            Qt.RoundCap, Qt.RoundJoin
        )
        for fig in self.fig_lst:
            if fig["figure"] == "rectangle":
                self.draw_single_rectangle(
                    fig["x1"] - self.corn_x, fig["y1"] - self.corn_y,
                    fig["x2"] - self.corn_x, fig["y2"] - self.corn_y
                )

            elif fig["figure"] == "circumference":
                self.draw_single_circumference(
                    fig["x1"] - self.corn_x, fig["y1"] - self.corn_y,
                    fig["x2"] - self.corn_x, fig["y2"] - self.corn_y
                )

            elif fig["figure"] == "ellipse":
                self.draw_single_ellipse(
                    fig["x1"] - self.corn_x, fig["y1"] - self.corn_y,
                    fig["x2"] - self.corn_x, fig["y2"] - self.corn_y
                )

        if self.pressed == True:
            if self.parent.drawing_mode == "rect":
                self.draw_single_rectangle(
                    self.rc_x1, self.rc_y1, self.rc_x2, self.rc_y2
                )

            elif self.parent.drawing_mode == "circ":
                self.draw_single_circumference(
                    self.rc_x1, self.rc_y1, self.rc_x2, self.rc_y2
                )

            elif self.parent.drawing_mode == "elli":
                self.draw_single_ellipse(
                    self.rc_x1, self.rc_y1, self.rc_x2, self.rc_y2
                )


    def undo_last(self):
        try:
            #del self.fig_lst[-1]
            self.fig_lst.pop()
            self.re_draw_me()
            self.new_list.emit(self.fig_lst)

        except IndexError:
            print("reached the last of the mohicans")

    def mousePressEvent(self, event):
        ev_pos = event.scenePos()
        print("mousePressEvent")
        x, y = ev_pos.x(), ev_pos.y()
        print("x, y =", x, y)
        self.x_ini, self.y_ini = x, y
        self.pressed = True

    def mouseReleaseEvent(self, event):
        print("mouseReleaseEvent")
        self.pressed = False

        if self.parent.drawing_mode == "rect":
            if self.rc_x1 > self.rc_x2:
                self.rc_x1, self.rc_x2 = self.rc_x2, self.rc_x1

            if self.rc_y1 > self.rc_y2:
                self.rc_y1, self.rc_y2 = self.rc_y2, self.rc_y1

            self.fig_lst.append(
                {
                    "figure"       : "rectangle",
                    "x1" : self.rc_x1 + self.corn_x, "y1" : self.rc_y1 + self.corn_y,
                    "x2" : self.rc_x2 + self.corn_x, "y2" : self.rc_y2 + self.corn_y
                }
            )

        elif self.parent.drawing_mode == "circ":
            self.fig_lst.append(
                {
                    "figure"       : "circumference",
                    "x1" : self.rc_x1 + self.corn_x, "y1" : self.rc_y1 + self.corn_y,
                    "x2" : self.rc_x2 + self.corn_x, "y2" : self.rc_y2 + self.corn_y
                }
            )

        elif self.parent.drawing_mode == "elli":
            self.fig_lst.append(
                {
                    "figure"       : "ellipse",
                    "x1" : self.rc_x1 + self.corn_x, "y1" : self.rc_y1 + self.corn_y,
                    "x2" : self.rc_x2 + self.corn_x, "y2" : self.rc_y2 + self.corn_y
                }
            )

        self.new_list.emit(self.fig_lst)

    def mouseMoveEvent(self, event):
        if self.pressed == True:
            ev_pos = event.scenePos()
            self.rc_x2, self.rc_y2 = ev_pos.x(), ev_pos.y()
            self.rc_x1 = self.x_ini
            self.rc_y1 = self.y_ini
            self.re_draw_me()


class exlud_mark(QObject):
    ended = Signal(list)
    canceled = Signal()
    def __init__(
        self, parent = None, img_arr = None,
        in_x1 = 0, in_y1 = 0, palette_stat = (False, False)
    ):
        super(exlud_mark, self).__init__(parent)

        self.img_arr = img_arr
        sqrt_stat, inve_stat = palette_stat[0], palette_stat[1]

        scale_diff = parent.curent_zoom_scale

        print("scale_diff =", scale_diff)

        my_code_path = os.path.dirname(os.path.abspath(__file__))
        ui_path = my_code_path + os.sep + "ex_mask.ui"
        print("ui_path = ", ui_path)
        self.dialog = QtUiTools.QUiLoader().load(ui_path)
        self.dialog.setWindowTitle("Exluding Marking")
        self.dialog.show()

        self.dialog.OkButton.clicked.connect(self.on_ok_click)
        self.dialog.UndoButton.clicked.connect(self.on_undo_click)
        self.dialog.CancelButton.clicked.connect(self.on_cancel_click)

        self.dialog.radioButtonRect.toggled.connect(self.btnstate)
        self.dialog.radioButtonCirc.toggled.connect(self.btnstate)
        self.dialog.radioButtonEllip.toggled.connect(self.btnstate)

        self.dialog.radioButtonRect.setChecked(True)

        self.my_scene = ExludImgagSene(self)
        self.dialog.graphicsView.setScene(self.my_scene)
        self.dialog.graphicsView.setDragMode(QGraphicsView.NoDrag)

        bmp_heat = np2bmp_heat()
        rgb_np = bmp_heat.img_2d_rgb(
            data2d = self.img_arr, invert = inve_stat,
            sqrt_scale = sqrt_stat, i_min_max = [None, None]
        )

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )
        tmp_pixmap = QPixmap.fromImage(q_img)

        self.my_scene.set_pix_map(tmp_pixmap, in_x1, in_y1)
        self.my_scene.draw_pix_map()
        amplif = 1.5
        scale_amplif = scale_diff * amplif
        self.dialog.graphicsView.scale(scale_amplif, scale_amplif)

        print("self.dialog.graphicsView.rect() =", self.dialog.graphicsView.rect())
        print("scale_amplif =", scale_amplif)

        b_rect = self.dialog.graphicsView.mapToScene(
            self.dialog.graphicsView.viewport().geometry()
        ).boundingRect()
        b_rect = b_rect.getRect()
        print("b_rect =", b_rect)

        self.my_scene.new_list.connect(self.update_lst)

    def on_ok_click(self):
        self.ended.emit(self.full_list)
        print("OkButton clicked")
        self.dialog.close()

    def on_undo_click(self):
        self.my_scene.undo_last()
        print("UndoButton clicked")

    def on_cancel_click(self):
        self.canceled.emit()
        print("CancelButton clicked")
        self.dialog.close()

    def update_lst(self, new_list):
        self.full_list = new_list
        print("new_list =", self.full_list)

    def btnstate(self):
        if self.dialog.radioButtonRect.isChecked():
            print("mode = rectangle")
            self.drawing_mode = "rect"

        elif self.dialog.radioButtonCirc.isChecked():
            print("mode = circumference")
            self.drawing_mode = "circ"

        elif self.dialog.radioButtonEllip.isChecked():
            print("mode = Ellipse")
            self.drawing_mode = "elli"

        else:
            print("error mode")
            self.drawing_mode = None


if __name__ == '__main__':
    app = QApplication(sys.argv)
    x_size = 900
    y_size = 700

    x_ax = np.arange(x_size)
    y_ax = np.arange(y_size)
    pi_2 = 3.14159235358 * 2.0
    sx = 1.0-(np.cos(x_ax * pi_2 / x_size))
    sy = 1.0-(np.cos(y_ax * pi_2 / y_size))
    xx, yy = np.meshgrid(sx, sy, sparse = True)
    np_full_img = xx + yy
    np_full_img = 50 * (np_full_img / np_full_img.max())

    form = exlud_mark(None, np_full_img)
    sys.exit(app.exec_())

