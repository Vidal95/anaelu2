
program img_ex_mark

    use input_arg_tools,                only: handling_kargs, arg_def_list
    use edf_utils,                      only: get_efd_info, write_edf
    use inner_tools,                    only: mark_mask, img_out

    implicit none

    character(len=9999)                             :: str_data
    real(kind = 8), dimension(:,:), allocatable     :: img_in
    integer(kind = 8)                               :: x1, x2, y1, y2

    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 4
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "img_in"
    my_def%field(1)%value = "my_img_in.edf"

    my_def%field(2)%var_name = "img_out"
    my_def%field(2)%value = "marked_img.edf"

    my_def%field(3)%var_name = "rect_x1_x2_y1_y2"
    my_def%field(3)%value = "900,900,1600,1600"

    my_def%field(4)%var_name = "figure"
    my_def%field(4)%value = "circumference"

    call handling_kargs(my_def)

    write(*,*) "img_in: ", my_def%field(1)%value
    write(*,*) "img_out: ", my_def%field(2)%value
    write(*,*) "rect_x1_x2_y1_y2: ", my_def%field(3)%value
    write(*,*) "figure: ", my_def%field(4)%value

    !TODO the next line only works when the params in CLI are explisitly integer
    read(my_def%field(3)%value,*) x1, x2, y1, y2
    write(*,*) "rect_x1_x2_y1_y2: " , x1, x2, y1, y2

    call get_efd_info(my_def%field(1)%value, str_data, img_in)
    write(*,*) "header: ", trim(str_data)

    call mark_mask(img_in, x1, x2, y1, y2, my_def%field(4)%value)
    call write_edf(my_def%field(2)%value, img_out)

end program img_ex_mark
