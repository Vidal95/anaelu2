echo "removing previous binrs, objs and imgs"
rm anaelu_img_add img_sum.edf
rm anaelu_img_add.exe
rm *.o *.mod

echo "compiling"
gfortran -c -O2 -Wall input_args.f90
gfortran -c -O2 -Wall rw_n_use_edf.f90
gfortran -c -O2 -Wall tools.f90

gfortran -c -O2 -Wall anaelu_img_add.f90

echo "linking"
gfortran -o anaelu_img_add.exe *.o -static

echo "running binary exe"
./anaelu_img_add.exe img_64bit_xrd.edf img_smooth.edf img_sum.edf

rm *.o *.mod
