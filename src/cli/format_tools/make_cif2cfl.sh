#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC=-I"$CRYSFML/GFortran64/LibC"
LIB=-L"$CRYSFML/GFortran64/LibC"
LIBSTATIC=" -static -lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly

OPT1="-c -O2 -ffree-line-length-none -funroll-loops" # same as CRYSFML

#OPT1="-c -g -fbacktrace -ffree-line-length-none"

echo "cif2cfl >> removing traces of previous runs"
rm anaelu_cif2cfl.exe
rm *cfl
rm *.mod

echo "cif2cfl >> Program Compilation -static"
gfortran $OPT1              ../cli_f90_deps/input_args.f90
gfortran $OPT1 "$INC"                            cif2cfl.f90
echo "Linking"
gfortran -o anaelu_cif2cfl.exe *.o  "$LIB" $LIBSTATIC
rm *.o
echo "running"
echo " "

#rm *.mod
echo "tst 1"
./anaelu_cif2cfl.exe
echo "tst 2"
./anaelu_cif2cfl.exe Pt_COD.cif aaaaPt.cfl
echo "tst 3"
./anaelu_cif2cfl.exe cif_in=Pt_COD.cif cfl_out=tst.cfl
echo "tst 4"

