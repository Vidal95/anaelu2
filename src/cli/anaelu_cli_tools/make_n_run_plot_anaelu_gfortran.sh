#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo " Cleaning"
rm anaelu_calc_xrd.exe                            #removing previous run
rm 2d_pat_1.asc img_file.edf img_file.raw         #removing data from previous run
rm 1d_pf.dat                                      #removing same as before
echo " Building"                                  #break point print
export CRYSFML=../crysfml_snapshot_nov_2017              #lib link dependencies
./make_anaelu_gfortran.sh                         #calling compile script
echo "Running"                                    #break point print
./anaelu_calc_xrd.exe dat_in=my_params.dat cfl_in=cfl_test.cfl edf_out=img_file.edf raw_out=img_file.raw  #running
echo "Plotting"                                   #break point print
#python3 matplot_2d_bin.py                         #show graph
python3 matplot_2d_pat_edf.py                      #show graph 2
